# docker-squid-alpine #

[![build status](https://gitlab.com/andrewheberle/docker-squid-alpine/badges/master/build.svg)](https://gitlab.com/andrewheberle/docker-squid-alpine/commits/master)

## Description ##

Squid proxy image derived from "gitlab.com/andrewheberle/docker-base-confd"

## Features ##

 - Looks up "peers" in a Docker Swarm and adds them as cache_peers.
 
 - SSL Bump/Peek/Splice


